package com.ideais.stock.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.ideais.stock.domain.Category;
import com.ideais.stock.json.CategoryJSON;
import com.ideais.stock.service.CategoryService;

@Path("/category")
public class CategoryWS {
	
	@Autowired
	private CategoryService categoryService;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CategoryJSON> getCategories() {
		List<CategoryJSON> categoryJSONs = new ArrayList<CategoryJSON>();
		
		for (Category category : categoryService.findAll()) {
			categoryJSONs.add(new CategoryJSON(category));
		}
		return categoryJSONs;
	}

	@Path("/{id}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public CategoryJSON getCategoryById(@PathParam("id") Long id) {
		return new CategoryJSON(categoryService.findById(id));
	}
	
}
