package com.ideais.stock.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDao<T> {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session session() {
		return sessionFactory.getCurrentSession();
	}

	protected T save(T entity) {
		session().saveOrUpdate(entity);
		return entity;
	}

	protected void delete(T entity) {
		session().delete(session().merge(entity));
	}

	@SuppressWarnings("unchecked")
	protected T findById(Class<? extends Object> persistenceClass, Long id) {
		return (T) session().get(persistenceClass, id);
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByRestrictions(
			Class<? extends Object> persistenceClass,
			List<Criterion> restrictions) {
		Criteria criteria = session().createCriteria(persistenceClass);
		for (Criterion restriction : restrictions) {
			criteria.add(restriction);
		}
		return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
	}

	@SuppressWarnings("unchecked")
	protected List<T> findAll(Class<T> persistenceClass) {
		return session().createCriteria(persistenceClass)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@SuppressWarnings("unchecked")
	protected List<T> findAll(Class<T> persistenceClass, Order orderBy) {
		return session().createCriteria(persistenceClass).addOrder(orderBy)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@SuppressWarnings("unchecked")
	protected List<T> findAll(Class<T> persistenceClass, Order orderBy,
			List<Criterion> restrictions) {
		Criteria criteria = session().createCriteria(persistenceClass);
		for (Criterion restriction : restrictions) {
			criteria.add(restriction);
		}
		return criteria.addOrder(orderBy)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@SuppressWarnings("unchecked")
	public List<T> findByParams(Class<T> persistenceClass,
			List<Criterion> restrictions, Boolean active)
			throws HibernateException {

		Criteria criteria = session().createCriteria(persistenceClass);

		for (Criterion restriction : restrictions) {
			criteria.add(restriction);
		}

		criteria.add(Restrictions.like("active", active));

		return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	public int getCount(Boolean active, String table) throws HibernateException {

		return ((BigInteger) session()
				.createSQLQuery("SELECT COUNT(CD_" + table + ") FROM " + table + " WHERE BO_ATIVO=" + active).list().get(0)).intValue();
	}
	
	public int getCount(Boolean active, String table, String query)	throws HibernateException {
		String getCountQuery = "SELECT COUNT(CD_" + table + ") FROM " + table + " WHERE BO_ATIVO=" + active;
		
		String[] splittedQuery = query.split(" ");
		
		getCountQuery = getCountQuery.concat(" AND (" ); 
		
		for (int i = 0; i < splittedQuery.length ; i++ ) {
			if (i == 0)
				getCountQuery = getCountQuery.concat(" NM_NOME like \"%"+splittedQuery[i]+"%\"");
			else
				getCountQuery = getCountQuery.concat(" OR NM_NOME like \"%"+splittedQuery[i]+"%\"");
			if ("PRODUTO".equals(table))
				getCountQuery = getCountQuery.concat(" OR NM_DESCRICAO_LONGA like \"%"+splittedQuery[i]+"%\"");
		}
		
		getCountQuery = getCountQuery.concat(")" );
		
		return ((BigInteger) session().createSQLQuery(getCountQuery).list().get(0)).intValue();
	}

}
