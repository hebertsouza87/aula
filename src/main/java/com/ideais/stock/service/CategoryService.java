package com.ideais.stock.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

import com.ideais.stock.dao.CategoryDao;
import com.ideais.stock.domain.Category;

public class CategoryService {
	
	static final Logger LOG = Logger.getLogger(CategoryService.class);

	@Autowired
	CategoryDao categoryDao;

	public Category save(Category category) {
		if (category.getId() == null) {
			return create(category);
		}
		return update(category);
	}
	
	private Category create(Category category) {
		if(category.getActive() == null)
			category.setActive(false);
		try {
			return categoryDao.save(category);
		} catch (HibernateException e) {
			LOG.error("Error ao salvar a categoria ", e);
			return null;
		}
	}

	private Category update(Category category) {
		Category categoryToBeEdited = categoryDao.findById(category.getId());
		
		if (categoryToBeEdited == null) {
			return null;
		}
		
		categoryToBeEdited.setActive(category.getActive());
		categoryToBeEdited.setName(category.getName());
		
		if(category.getActive() == null)
			categoryToBeEdited.setActive(false);
		
		try {
			return categoryDao.save(categoryToBeEdited);
		} catch (HibernateException e) {
			LOG.error("Error ao salvar a categoria ", e);
			return null;
		}
	}

	public Category findById(Long id) {
		try {
			return categoryDao.findById(id);
		} catch (HibernateException e) {
			LOG.error("Error ao pegar a categoria ("+id+")", e);
			return null;
		}
	}

	public List<Category> findAll() {
		try {
			return categoryDao.findAll();
		} catch (HibernateException e) {
			LOG.error("Error ao pegar todas as categorias ", e);
			return null;
		}
	}
	

}
