package com.ideais.stock.action;

import com.ideais.stock.domain.Category;
import com.ideais.stock.service.CategoryService;
import com.opensymphony.xwork2.ActionSupport;

public class CategoryAction extends ActionSupport {

	private static final long serialVersionUID = -4699966412143037968L;
	
	private Category category = new Category();
	private CategoryService categoryService = new CategoryService();


	public String saveCategory() {

		Category savedCategory = categoryService.save(category);

		if (savedCategory == null) {
			return ERROR;
		}

		return SUCCESS;
	}
}
